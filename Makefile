CC = g++
ODIR = build/obj
SDIR = src
INC = -Iinclude

_OBJS = Third_Poly_Root.o Precise_Root.o Main.o
OBJS = $(patsubst %,$(ODIR)/%,$(_OBJS))


$(ODIR)/%.o: $(SDIR)/%.cpp
	mkdir -p build/obj
	$(CC) -c $(INC) -o $@ $< $(CFLAGS) 

findroot: $(OBJS)
	g++ -o build/findroot $^

.PHONY: clean
clean:
	rm -R build