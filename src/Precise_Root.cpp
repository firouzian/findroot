/* This is Functions for find the root of given poly
 * every precise. There is two method implimented, First 
 * is Newton-Raphson and Second is Bisection. 
 * Written By: Amir Hossein Firouzian
 * Student No.: 95111005
 */

#include <iostream>
#include <cmath>	//Only for using in Power Function
#include <cstdlib>
#include "Third_Poly_Root.h"
#include "Precise_Root.h"

/* This is the STANDARD Newton=Raphson Algorithim.
 * One point should be mention is, it DOESN'T check 
 * the convergence rate.
 */
double Root_Netown(double X_Start,int No_Step){
	double X_Temp=0;
	for (int Loop_Counter_1=1;Loop_Counter_1<=No_Step;Loop_Counter_1++){
		double Tangent=Poly_Third_Degree_Tangent(X_Start);
		if (Tangent==0)
			exit(1);
		else
			X_Temp=X_Start-Poly_Third_Degree(X_Start)/Tangent;
		
		X_Start=X_Temp;
	}
	return X_Temp;
}

double Root_Bisection(double X_Begin, double X_End, int No_Step){
	double X_Mid=0;
	if (Is_Root(X_Begin,X_End-X_Begin)){
		for (int Loop_Counter_1=1;Loop_Counter_1<=No_Step;Loop_Counter_1++){
			X_Mid=(X_Begin+X_End)/2;
			if (Is_Root(X_Begin, X_Mid-X_Begin))
				X_End=X_Mid;
			else
				X_Begin=X_Mid;
		}
	}else
		exit(0);
	return X_Mid;
}

/* I use following link in order to Impliment the Secant Method:
 * https://en.wikipedia.org/w/index.php?title=Special:CiteThisPage&page=Secant_method&id=719572753
 */
double Root_Secant(double X_1, double X_2, int No_Step){
	double X_Temp;
	for (int Loop_Counter_1=1; Loop_Counter_1<=No_Step; Loop_Counter_1++){
		X_Temp=X_2-Poly_Third_Degree(X_2)*((X_2-X_1)/
			(Poly_Third_Degree(X_2)-Poly_Third_Degree(X_1)));
		X_1=X_2;
		X_2=X_Temp;
	}
	return X_Temp;
}