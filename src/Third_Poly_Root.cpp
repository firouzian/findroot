/* This is Functions for FindRoot Program
 * Written By: Amir Hossein Firouzian
 * Student No.: 95111005
 */

#include <iostream>
#include <cmath>	//Only for using in Power Function
#include <cstdlib>
#include "Third_Poly_Root.h"

#define Derive_Interval .001

void Find_Root(double X_Begin, double X_Final, double Interval){
	double Pivot=X_Begin;
	
	// Pivot is to show where we left of in searching
	while(Pivot <= X_Final){
		if (Is_Root(Pivot,Interval))
			std::cout << "One root from " << Pivot
						<< " To " << Pivot+Interval << std::endl;
		
		// Next begin point
		Pivot+=Interval;
	}
}

/* The purpose of belowing function is to 
 * check whether there is root in specific
 * Intervals or not!
 */
bool Is_Root(double X_Begin, double X_Interval){
	if (Poly_Third_Degree(X_Begin)*
		Poly_Third_Degree(X_Begin+X_Interval) <=0)
			return true;
	else
		return false;
}

/* Calculate the function based on given
 * constants that hard coded in source
 */
double Poly_Third_Degree(double X_Input){
	double Poly_Constants[4]={1,-3,3,-1};
	return Poly_Constants[0]*std::pow(X_Input,3)+
			Poly_Constants[1]*std::pow(X_Input,2)+
			Poly_Constants[2]*std::pow(X_Input,1)+
			Poly_Constants[3];
}

/* For derivative of Poly, I arbitrarily set my 
 * interval to 0.001, So in this sence the return
 * value is 1000 time grater than the diffrence 
 * between result of two point.
 */
double Poly_Third_Degree_Tangent(double X_Input){
	return (Poly_Third_Degree(X_Input+Derive_Interval)-
		Poly_Third_Degree(X_Input))/Derive_Interval;
}