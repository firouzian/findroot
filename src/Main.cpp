/* This code is written by Amir Hossein Firouzian
 * Student No.: 95111005
 * 
 * This code publish under GPLv3 License That give loyalty free to
 * make change or publish until have this note on code.
 * Please refer to https://www.gnu.org/licenses/gpl-3.0.en.html for more info
 */

#include <iostream>
#include <cmath>	//Only for using in Power Function
#include <cstdlib>
#include <vector>
#include "Third_Poly_Root.h"
#include "Precise_Root.h"


int main(int Argument_Numbers,char *Argument_Vec[]){
	
	// Check for arguments that bash pass
	if (Argument_Numbers!=4){
		std::cout << "This program give exactly 3 number as Input" 
					<< std::endl;
		exit(1);
	}
	if (std::atof(Argument_Vec[1])>=std::atof(Argument_Vec[2])){
		std::cout << "Begin is lager than End" 
					<< std::endl;
		exit(2);
	}
	
	std::cout << "Newton's Method:" << std::endl;
	std::cout << Root_Netown(std::atof(Argument_Vec[1]),
							 std::atof(Argument_Vec[3])) << std::endl;
	std::cout << "Bisection Method" << std::endl;
	std::cout << Root_Bisection(std::atof(Argument_Vec[1]),
								std::atof(Argument_Vec[2]),
								std::atof(Argument_Vec[3])) << std::endl;
	std::cout << "Secant Method" << std::endl;
	std::cout << Root_Secant(std::atof(Argument_Vec[1]),
							 std::atof(Argument_Vec[2]),
							 std::atof(Argument_Vec[3])) << std::endl;
	
	exit(0);
}