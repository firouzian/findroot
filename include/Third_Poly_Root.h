double Poly_Third_Degree(double X_Input);
bool Is_Root(double X_Begin, double X_Interval);
void Find_Root(double X_Begin, double X_Final, double Interval);
double Poly_Third_Degree_Tangent(double X_Input);